package bj;
import java.util.Random;
public class Deck extends Card
{
	
	private Card[] myCards;
	private int cardCount;


	public Deck(int numberOfDecks)
	{
		super(1);
		this.cardCount = numberOfDecks*52;
		this.myCards = new Card[cardCount];
		int a = 0;
		for(int d = 0; d < numberOfDecks; d++)
		{
			for(int suit = 0; suit<4; suit++)
			{
				for(int number = 0; number <13; number++)
				{
					this.myCards[a] = new Card(suit, number);
					a++;
				}
			}
		}
	}
	
	public void shuffle()
	{
		Random random = new Random();
		Card temp;
		int index;
		
		for(int x = 0; x < cardCount; x++)
		{
			index = random.nextInt(cardCount);
			temp = myCards[x];
			myCards[x] = myCards[index];
			myCards[index] = temp;
		}
	}
	
	public Card dealCards()
	{
		
		Card top = myCards[0];
//		for(int a = 0; a < 2; a++)
//		{
			for(int b = 1; b < cardCount; b++)
			{
				myCards[b-1] = myCards[b];
			}
		
			myCards[cardCount-1] = null;
			cardCount--;
			System.out.println(top);
//		}
			
		
		return top;
	}
	
}
