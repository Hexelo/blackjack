package bj;

public class Card 
{
	private int aName, aSuit, cardValue, numberOfDecks;
	private String nameAndSuit = null;
//	protected String[] deck = null;

	
	private static String[] names = new String[] {"Ace", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Jack", "Queen", "King"};
	private static String[] suits = new String[] {"diamonds", "hearts", "spades", "clubs"};

	public Card(int nd)
	{
		this.numberOfDecks = nd;
	}
	
	public Card(int aName, int aSuit)
	{
		this.aName = aName;
		this.aSuit = aSuit;

	}
	
	
	public @Override String toString()
	{
		for(aSuit = 0; aSuit < suits.length; aSuit++)
		{
			for(aName = 0; aName < names.length; aName++)
			{
				if(aName == 0)
				{
					cardValue = 11;
				}
				if(aName == 1)
				{
					cardValue = 2;
				}
				if(aName == 2)
				{
					cardValue = 3;
				}
				if(aName == 3)
				{
					cardValue = 4;
				}
				if(aName == 4)
				{
					cardValue = 5;
				}
				if(aName == 5)
				{
					cardValue = 6;
				}
				if(aName == 6)
				{
					cardValue = 7;
				}
				if(aName == 7)
				{
					cardValue = 8;
				}
				if(aName == 8)
				{
					cardValue = 9;
				}
				if(aName >= 9 )
				{
					cardValue = 10;
				}
			
				nameAndSuit = names[aName] + " of " + suits[aSuit];
//				System.out.println(nameAndSuit + ": "+ cardValue);
			}
		}
			
		return nameAndSuit;
	}
	
	
	
	public int getCardValue()
	{
		return cardValue;
	}
	
	public int getaName() {
		return aName;
	}

	public int getaSuit()
	{
		return aSuit;
	}
	
	public String getNameAndSuit()
	{
		return nameAndSuit;
	}
	
	
}
