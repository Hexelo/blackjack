package bj;

public class Player 
{
	private String name;
	private Card[] hand = new Card[2];
	private int numberOfCards;
	
	public Player(String aName)
	{
		this.name = aName;
	}
	public boolean addCard(Card aDraw)
	{
		hand[numberOfCards] = aDraw;
		numberOfCards++;
		return (getHandSum() <= 21);
	}
	public int getHandSum()
	{
		int handSum = 0;
		int cardNum = 0;
		
		for(int i = 0; i < numberOfCards; i++)
		{
			cardNum = hand[i].getaName();
			cardNum = hand[i].getCardValue();
			handSum += cardNum;
		}
		return handSum;
	}
	public void printHand()
	{
		for(int i = 0; i < numberOfCards; i++)
		{
		System.out.println("Players cards: " + hand[i].toString());
		}
	}
}
